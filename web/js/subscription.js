$(document).ready(function() {

     $('.js-subscription-type').change(function() {

        var groupId = $(this).attr('data-group-id');
        var form = $('form[name="subscription_' + groupId + '"]');

        if ( form.length ) {
            $.post(form.attr('action'), form.serialize(), function(response) {
                if (response.status == 'success') {
                    console.log('success');
                } else {
                    console.log(response.status);
                }
            }, 'JSON');
        }

    });
});
