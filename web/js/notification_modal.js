$(document).ready(function() {

    $('#notifyModal').on('show.bs.modal', function (event) {

        var modal = $(this);
        // Button that triggered the modal
        var button = $(event.relatedTarget);
        var groupId = button.attr('data-group-id');
        $('input[name="notify_group_id_modal"]').val(groupId);
        var groupName = button.attr('data-group-name');
        modal.find('.modal-title').text(groupName);

        $('div.alert').text('');
        $('div.alert').hide();
    });

    $("#submit_notify_modal").click(function(event){
        event.preventDefault();

        // notify_group_id_modal
        // notify_action_url_modal
        // notify_email_text_modal
        // notify_sms_text_modal
        var groupId = $('input[name="notify_group_id_modal"]').val();
        var url = $('input[name="notify_action_url_modal"]').val();
        var email = $('textarea[name="notify_email_text_modal"]').val();
        var sms = $('textarea[name="notify_sms_text_modal"]').val();

        if ( groupId && url && email && sms) {
            $.post(url, {group_id: groupId, email_txt: email, sms_txt: sms}, function(response) {
                if (response.status == 'success') {
                    console.log('success');
                    $('#notifyModal').modal('hide');
                } else {
                    console.log(response.status);
                    $('div.alert').text(response.errors);
                    $('div.alert').show();
                }
            }, 'JSON');
        }
    });

});

