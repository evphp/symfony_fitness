<?php

namespace VEV\FitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vev_fitness_confirmation")
 */
class Confirmation
{
    /**
     * @var int $userId
     *
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @var string $token
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $token;

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Confirmation
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Confirmation
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
