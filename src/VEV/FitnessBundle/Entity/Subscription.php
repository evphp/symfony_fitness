<?php

namespace VEV\FitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Workout;

/**
 * @ORM\Entity
 * @ORM\Table(name="vev_fitness_subscription", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="user_group", columns={"user_id", "group_id"})
 * })
 * @ORM\Entity(repositoryClass="VEV\FitnessBundle\Repository\SubscriptionRepository")
 */
class Subscription
{
    const EMAIL = 1;
    const SMS   = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var int $userId
     *
     * @ORM\Column(name="user_id", type="integer")
     * @Assert\NotBlank()
     */
    protected $userId;

    /**
     * @var int $groupId
     *
     * @ORM\Column(name="group_id", type="integer")
     * @Assert\NotBlank()
     */
    protected $groupId;

    /**
     * @var smallint $type
     *
     * @ORM\Column(type="smallint", length=1, options={"comment":"Avaliable types: 1 - email, 2 - phone"})
     */
    protected $type;

    /**
     * @var Entity $user
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
     protected $user;

    /**
     * @var Entity $group
     *
     * Many Subscriptions can have one Workout group
     * @ORM\ManyToOne(targetEntity="Workout", inversedBy="subscriptions")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
     protected $group;

    /**
     * Конструктор класса Subscription
     */
    public function __construct(User $user, Workout $workout)
    {
        $this->user = $user;
        $this->setUserId($user->getId());

        $this->group = $workout;
        $this->setGroupId($workout->getId());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Subscription
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     *
     * @return Subscription
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Subscription
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param \VEV\FitnessBundle\Entity\User $user
     *
     * @return Subscription
     */
    public function setUser(\VEV\FitnessBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \VEV\FitnessBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \VEV\FitnessBundle\Entity\Workout $group
     *
     * @return Subscription
     */
    public function setGroup(\VEV\FitnessBundle\Entity\Workout $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \VEV\FitnessBundle\Entity\Workout
     */
    public function getGroup()
    {
        return $this->group;
    }
}
