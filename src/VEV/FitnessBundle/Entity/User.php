<?php

namespace VEV\FitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="vev_fitness_user")
 * @ORM\Entity(repositoryClass="VEV\FitnessBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var string $username
     *
     * @ORM\Column(type="string", length=60, unique=true, nullable=true)
     */
    protected $username;

    /**
     * @Assert\Length(min=5, max=4096)
     */
    private $plainPassword;

    /**
     * @var string $password
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $password;

    /**
     * @var string $salt
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $salt;

    /**
     * @var boolean $emailConfirmed
     *
     * @ORM\Column(name="email_confirmed", type="boolean")
     */
    private $emailConfirmed;

    /**
     * @var boolean $isActive
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var ArrayCollection $userRoles
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(
     * name="user_role",
     * joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * },
     * inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * }
     * )
     */
    protected $userRoles;

    /**
     * Для пользователей с ролью USER_ROLE
     * username = email
     * Для пользователей с ролью ADMIN_ROLE
     * username = username_force
     */
    protected $username_force;

    /**
     * @var Entity $info
     *
     * @ORM\OneToOne(targetEntity="UserInfo", cascade={"persist", "remove", "refresh"}, orphanRemoval=true)
     * @Assert\Valid()
     */
     protected $info;

    /**
     * Конструктор класса User
     */
    public function __construct()
    {
        $this->isActive = false;
        $this->emailConfirmed = false;
        $this->userRoles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     *
     * @ORM\PrePersist
     */
    public function setUsername()
    {
        $userRoles = $this->getRoles();
        $username = $this->email;

        if ($this->username_force) {
            foreach ($userRoles as $role) {
                $curr_role = $role->getRole();
                if ($role->getRole() == 'ROLE_ADMIN') {
                    $username = $this->username_force;
                }
            }
        }

        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * Get plainPassword
     *
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set plainPassword
     *
     * @param string $password
     *
     * @return User
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get salt
     *
     * @return string The salt.
     */
    public function getSalt()
    {
        return $this->salt;
    }
 
    /**
     * Set salt
     * implement abstract method of UserInterface
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Erase credentials
     * implement abstract method of UserInterface
     */
    public function eraseCredentials()
    {
    }

    /**
     * Get userRoles
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * Get roles
     * 
     * @return array An array of Role objects
     */
    public function getRoles()
    {
        return $this->getUserRoles()->toArray();
    }

    public function getUsernameForce()
    {
        return $this->username_force;
    }

    public function setUsernameForce($username)
    {
        $this->username_force = $username;
    }

    /**
     * Сравнивает пользователей, если это один и тот же
     * человек возвращает true
     * 
     * @param UserInterface $user The user
     *
     * @return boolean True if equal, false othwerwise.
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     *
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add userRole
     *
     * @param \VEV\FitnessBundle\Entity\Role $userRole
     *
     * @return User
     */
    public function addUserRole(\VEV\FitnessBundle\Entity\Role $userRole)
    {
        $this->userRoles[] = $userRole;

        return $this;
    }

    /**
     * Remove userRole
     *
     * @param \VEV\FitnessBundle\Entity\Role $userRole
     */
    public function removeUserRole(\VEV\FitnessBundle\Entity\Role $userRole)
    {
        $this->userRoles->removeElement($userRole);
    }

    /**
     * Set emailConfirmed
     *
     * @param boolean $emailConfirmed
     *
     * @return User
     */
    public function setEmailConfirmed($emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    /**
     * Get emailConfirmed
     *
     * @return boolean
     */
    public function getEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    // next functions implements AdvancedUserInterface interface
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    // next functions implements \Serializable interface

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->password,
            $this->isActive,
            $this->emailConfirmed,
            $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->emailConfirmed,
            $this->salt,
        ) = unserialize($serialized);
    }

    /**
     * Set info
     *
     * @param \VEV\FitnessBundle\Entity\UserInfo $info
     *
     * @return User
     */
    public function setInfo(\VEV\FitnessBundle\Entity\UserInfo $info = null)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return \VEV\FitnessBundle\Entity\UserInfo
     */
    public function getInfo()
    {
        return $this->info;
    }
}
