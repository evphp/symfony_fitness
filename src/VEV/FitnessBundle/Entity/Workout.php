<?php

namespace VEV\FitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="vev_fitness_workout")
 * @ORM\Entity(repositoryClass="VEV\FitnessBundle\Repository\WorkoutRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Workout
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message="Название не добавлено")
     */
    protected $name;

    /**
     * @var string $trainer
     *
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message="Тренер не добавлен")
     */
    protected $trainer;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Описание не добавлено")
     */
    protected $description;

    /**
     * @var DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var ArrayCollection $subscriptions
     *
     * One Workout has many Subscriptions
     * @ORM\OneToMany(targetEntity="Subscription", mappedBy="group", cascade={"remove"}, orphanRemoval=true)
     */
     protected $subscriptions;

    /**
     * Конструктор класса Workout
     */
    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Workout
     *
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Workout
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set trainer
     *
     * @param string $trainer
     *
     * @return Workout
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return string
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Workout
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Add subscription
     *
     * @param \VEV\FitnessBundle\Entity\Subscription $subscription
     *
     * @return Workout
     */
    public function addSubscription(\VEV\FitnessBundle\Entity\Subscription $subscription)
    {
        $this->subscriptions[] = $subscription;

        return $this;
    }

    /**
     * Remove subscription
     *
     * @param \VEV\FitnessBundle\Entity\Subscription $subscription
     */
    public function removeSubscription(\VEV\FitnessBundle\Entity\Subscription $subscription)
    {
        $this->subscriptions->removeElement($subscription);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * Get subscriptions as array
     * 
     * @return array An array of Subscription objects
     */
    public function getSubscriptionsArray()
    {
        return $this->getSubscriptions()->toArray();
    }
}
