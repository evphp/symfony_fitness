<?php

namespace VEV\FitnessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="vev_fitness_userinfo")
 */
class UserInfo
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $fullname
     *
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank(message="Ф.И.О. не указано")
     */
    protected $fullname;

    /**
     * @var string $gender
     *
     * @ORM\Column(type="string", length=6, columnDefinition="enum('male', 'female')")
     * @Assert\NotBlank(message="Пол не выбран")
     */
    protected $gender;

    /**
     * @var Date $birthday
     *
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $birthday;

    /**
     * @var string $phone
     *
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^\+?([0-9]{5,20})$/",
     *     match=true,
     *     message="Недопустимый формат телефонного номера"
     * )
     */
    protected $phone;

    /**
     * @var string $image
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var string $imageTmp
     *
     * @Assert\File(mimeTypes={ "image/jpeg" })
     * @Assert\Image(
     *     minWidth = 200,
     *     maxWidth = 800,
     *     minHeight = 200,
     *     maxHeight = 800
     * )
     */
    protected $imageTmp;

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserInfo
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return UserInfo
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return UserInfo
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return UserInfo
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return UserInfo
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return UserInfo
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imageTmp
     *
     * @param string $imageTmp
     *
     * @return UserInfo
     */
    public function setImageTmp($imageTmp)
    {
        $this->imageTmp = $imageTmp;

        return $this;
    }

    /**
     * Get imageTmp
     *
     * @return string
     */
    public function getImageTmp()
    {
        return $this->imageTmp;
    }
}
