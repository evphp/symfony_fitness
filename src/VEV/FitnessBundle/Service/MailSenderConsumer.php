<?php

namespace VEV\FitnessBundle\Service;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NotificationConsumer
 */
class MailSenderConsumer implements ConsumerInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EntityManager $entityManager
     */
    private $em;

    /**
     * @var ContainerInterface $container
     */
    private $container;
 
    public function __construct(\Swift_Mailer $mailer, $entityManager, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->em = $entityManager;
        $this->container = $container;
    }

    /**
     * @var AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $msg)
    {
        //echo 'Try to send message somewhere : '.$msg->getBody().PHP_EOL;
        echo 'Try to send message somewhere...'.PHP_EOL;
        $msgArgs = json_decode($msg->getBody(), true);

        $emailTo = $msgArgs['email'];
        $msgText = $msgArgs['message'];

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($emailTo)
            ->setBody(
                $msgText,
                'text/html'
            );
        $this->mailer->send($message);

        echo 'Email was sent to : '. $emailTo.PHP_EOL;
    }
}