<?php

namespace VEV\FitnessBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use VEV\FitnessBundle\Entity\Workout;
use VEV\FitnessBundle\Entity\Subscription;

class NotificationManager
{
    /**
     * @var EntityManager $entityManager
     */
    private $em;

    /**
     * @var ContainerInterface $container
     */
    private $container;

    public function __construct($entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function task($workoutId, $emailTextRaw, $smsTextRaw)
    {

        $workout = $this->em
            ->getRepository(Workout::class)
            ->find($workoutId);
        $ubscriptions = $workout->getSubscriptionsArray();

        foreach ($ubscriptions as $ubscription) {
            $user = $ubscription->getUser();

            if ($user->getIsActive()) {
                
                switch ($ubscription->getType()) {
                    case Subscription::EMAIL:
                        $emailText = str_replace("%email%", $user->getEmail(), $emailTextRaw);
                        $emailText = str_replace("%fullname%", $user->getInfo()->getFullname(), $emailText);
                        $emailText = str_replace("%birthday%", $user->getInfo()->getBirthday()->format('Y-m-d'), $emailText);
                        $this->container->get('old_sound_rabbit_mq.send_email_producer')
                            ->publish(json_encode(['email' => $user->getEmail(), 'message' => $emailText ]));
                        break;
                    case Subscription::SMS:
                        $smsText = str_replace("%email%", $user->getEmail(), $smsTextRaw);
                        $smsText = str_replace("%fullname%", $user->getInfo()->getFullname(), $smsText);
                        $smsText = str_replace("%birthday%", $user->getInfo()->getBirthday()->format('Y-m-d'), $smsText);
                        $this->container->get('old_sound_rabbit_mq.send_sms_producer')
                            ->publish(json_encode(['phone' => $user->getInfo()->getPhone(), 'message' => $smsText ]));
                        break;
                    }
            }
        }

        return true;
    }
}