<?php

namespace VEV\FitnessBundle\Service;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NotificationConsumer
 */
class SmsSenderConsumer implements ConsumerInterface
{
    /**
     * @var EntityManager $entityManager
     */
    private $em;

    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var string $url
     */
    private $url;
 
    public function __construct($entityManager, $url, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->url = $url;
        $this->container = $container;
    }

    /**
     * @var AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $msg)
    {
        //echo 'Try to send message somewhere : '.$msg->getBody().PHP_EOL;
        echo 'Try to send SMS somewhere...'.PHP_EOL;
        $msgArgs = json_decode($msg->getBody(), true);

        $smsTo = $msgArgs['phone'];
        $msgText = $msgArgs['message'];

        $url = $this->url;
        $url .= '?phone='.urlencode($smsTo).'&message='.urlencode($msgText);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, true); 

        $curlResult = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code != 200) {
            # отправляем сообщения в очередь _delayed сообщения из которой никогда не будут обработаны
            # поскольку для нее не назначен consumer, время жизни сообщений в этой очереди 10 мин
            # по истечении которого сообщение попадает в основную очередь, которая будет обработана
            # данным consumer'ом
            $this->container->get('old_sound_rabbit_mq.delayed_send_sms_producer')
                ->publish(json_encode(['phone' => $smsTo, 'message' => $msgText ]));
            echo 'SMS REPEAT AFTER 10min'.PHP_EOL;
        }

        echo 'SMS was sent to : '. $smsTo.PHP_EOL;
    }
}