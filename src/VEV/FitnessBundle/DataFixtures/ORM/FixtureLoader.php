<?php

namespace VEV\FitnessBundle\DataFixtures\ORM;

use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
//use Doctrine\Common\DataFixtures\FixtureInterface;
//use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
 
class FixtureLoader extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create new role ROLE_ADMIN
        $adminRole = new Role();
        $adminRole->setName('ROLE_ADMIN');
        $manager->persist($adminRole);

        // create new role ROLE_USER
        $userRole = new Role();
        $userRole->setName('ROLE_USER');
        $manager->persist($userRole);
 
        // creare new user
        $user = new User();
        $user->setEmail('admin@fintess.com');
        $user->setIsActive(true);
        $user->setEmailConfirmed(true);
        //$user->setBirthday(new \DateTime('01-01-2000'));
        $user->setUsernameForce('admin');
        $user->setSalt(md5(time()));
 
        // create user passoword
        // $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        // $password = $encoder->encodePassword('admin', $user->getSalt());

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, 'admin');

        //$password = $encoder->encodePassword($user, 'admin');

        $user->setPassword($password);

        // add user role created above
        // $user->getUserRoles()->add($role);
        $user->addUserRole($adminRole);
 
        $manager->persist($user);
        $manager->flush();
    }
}