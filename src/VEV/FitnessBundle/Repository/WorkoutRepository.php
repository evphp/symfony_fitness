<?php

namespace VEV\FitnessBundle\Repository;

use Doctrine\ORM\EntityRepository;

class WorkoutRepository extends EntityRepository
{
    public function count()
    {
        return $this->createQueryBuilder('w')
            ->select('count(w.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getSubscribed()
    {
        return $this->createQueryBuilder('w')
            ->innerJoin('w.subscriptions', 's', 'WITH', 's.groupId = w.id')
            ->addGroupBy('s.groupId')
            ->orderBy('w.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
