<?php

namespace VEV\FitnessBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubscriptionRepository extends EntityRepository
{
    public function getSubsription($userId, $groupId)
    {
        return $this->createQueryBuilder('s')
            ->where('s.userId = :user_id AND s.groupId = :group_id')
            ->setParameter('user_id', $userId)
            ->setParameter('group_id', $groupId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function count()
    {
        return $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countWitActiveUsers()
    {
        return $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->innerJoin('s.user', 'u', 'WITH', 'u.isActive = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
