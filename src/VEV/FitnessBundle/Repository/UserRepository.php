<?php

namespace VEV\FitnessBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    // implements abstract method UserLoaderInterface
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getClients()
    {
        $userRole = 'ROLE_USER';

        return $this->createQueryBuilder('u')
            ->innerJoin('u.userRoles', 'r', 'WITH', 'r.name = :role')
            ->setParameter('role', $userRole)
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery()
            ->getResult(); //->getOneOrNullResult();
    }

    // false - count of all users (default)
    // true - count of is_active users
    public function count($isActive = false)
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.isActive = :is_active')
            ->setParameter('is_active', $isActive)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActive()
    {
        return $this->count(true);
    }

    public function countNotActive()
    {
        return $this->count(false);
    }
}
