<?php

namespace VEV\FitnessBundle\Form;

use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\UserInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
// use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array('label' => 'Е-почта', 'attr'=> array('class'=>'form-control')))
            ->add('isActive', CheckboxType::class,  array(
                'label' => 'Действующий', 
                'required' => false, 
                'attr'=> array('class'=>'form-control')
            ))
            ->add('info', UserInfoType::class,  array('label' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}