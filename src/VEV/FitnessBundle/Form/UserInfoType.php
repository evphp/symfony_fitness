<?php

namespace VEV\FitnessBundle\Form;

use VEV\FitnessBundle\Entity\UserInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, array('label' => 'Ф.И.О.', 'attr'=> array('class'=>'form-control')))
            ->add('birthday', DateType::class, array(
                'label' => 'Дата рождения', 
                'attr'=> array(),
                // 'placeholder' => [
                //     'year' => 'Year',
                //     'month' => 'Month',
                // ],
                'years' => range(date('Y')-60, date('Y')),
                'data' => new \DateTime('01-01-2000'),
            ))
            ->add('gender', ChoiceType::class, array(
                'label' => 'Пол', 
                'attr'=> array('class'=>'form-control'), 
                'choices'  => array(
                    '---'     => null,
                    'Мужской' => 'male',
                    'Женский' => 'female',
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Телефон', 
                'attr'=> array('class'=>'form-control'))
            )
            ->add('imageTmp', FileType::class, array(
                'label' => 'Фото',
                'required'=> false,
                //'attr'=> array('class'=>'form-control'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserInfo::class,
        ));
    }
}