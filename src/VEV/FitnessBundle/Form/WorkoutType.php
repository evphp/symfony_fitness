<?php

namespace VEV\FitnessBundle\Form;

use VEV\FitnessBundle\Entity\Workout;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class WorkoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $inputAttr = array('attr'=> array('class'=>'form-control'));
        $builder
            ->add('name', TextType::class, array('label' => 'Название.', 'attr'=> array('class'=>'form-control')))
            ->add('trainer', TextType::class, array('label' => 'Тренер', 'attr'=> array('class'=>'form-control')))
            ->add('description', TextType::class, array('label' => 'Описание', 'attr'=> array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Workout::class,
        ));
    }
}