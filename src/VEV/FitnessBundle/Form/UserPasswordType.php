<?php

namespace VEV\FitnessBundle\Form;

use VEV\FitnessBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $inputAttr = array('attr'=> array('class'=>'form-control'));
        $builder
            ->add('plainPassword', HiddenType::class, array(
                'data' => '213412353456634',
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array(
                    'label' => 'Пароль', 
                    'attr' => array('class'=>'form-control'),
                ),
                'second_options' => array(
                    'label' => 'Повторить пароль', 
                    'attr'=> array('class'=>'form-control'),
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}