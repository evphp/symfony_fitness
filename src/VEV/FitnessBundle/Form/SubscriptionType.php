<?php

namespace VEV\FitnessBundle\Form;

use VEV\FitnessBundle\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class SubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'label' =>  false,//'Подписаться',
                //'required'=> false,
                'attr'=> array('class'=>'form-control js-subscription-type'), 
                'choices'  => array(
                    'Не подписан' => null,
                    'По Е-почте' => Subscription::EMAIL,
                    'по СМС' => Subscription::SMS,
                )
            ))
            ->add('groupId', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Subscription::class,
        ));
    }
}