<?php

namespace VEV\FitnessBundle\Controller;

//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Confirmation;
use Symfony\Component\HttpFoundation\Request;
use VEV\FitnessBundle\Form\UserPasswordType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
 
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        $authUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('VEVFitnessBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error
        ));
    }

    public function confirmEmailAction($token, Request $request)
    {
        $token = addslashes(trim($token));

        // получаем сущность Confirmation по токену
        $confirmation = $this->getDoctrine()
            ->getRepository(Confirmation::class)
            ->findOneByToken($token);

        if ($confirmation) { 

            // токен Confirmation подтвержден

            // Дальнейшие этапы:
            // 1. находим пользователя
            // 2. создаем форму пароля
            // 3. проверяем форму и если верно :
            // 3.1. удаляем токен подтвердждения из БД
            // 3.2. устанавливаем пользователю пароль и активируем его
            // 3.3. создаем токен безопасни и добавляем его в систему
            // 3.4. добавляем токен безопасности в сессию
            // 3.5. перенаправляем пользователя на его страницу

            $userId = $confirmation->getUserId();

            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->find($userId);

            // если токен подтверждения верен и пользователь найден
            // то показываем форму смены пароля
            if ($user) {

                // создаем форму смены пароля
                $form = $this->createForm(UserPasswordType::class, $user);

                // обрабатываем запрос
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $em = $this->getDoctrine()->getManager();

                    // удаляем запись в таблице подтверждения регистрации
                    $em->remove($confirmation);

                    // добавляем пароль пользователя
                    $encoder = $this->get('security.password_encoder');
                    $password = $encoder->encodePassword($user, $user->getPlainPassword());
                    $user->setPassword($password);

                    // активируем пользователя
                    $user->setEmailConfirmed(true);
                    $user->setIsActive(true);

                    $em->persist($user);

                    $em->flush();

                    // "залогиниваем" пользователя
                    $userToken = new UsernamePasswordToken(
                        $user,
                        $user->getPassword(),
                        'main', // определяет тип провайдера безопасности, в системе м.б. несколько
                        $user->getRoles()
                    );
                    $this->get('security.token_storage')->setToken($userToken);
                    // устанавливаем переменную сессии, main - имя firewall в security.yml 
                    $this->get('session')->set('_security_main', serialize($token));


                    // пользователь подтвержден и активирован
                    // перенаправляем пользователя на его страницу
                    return $this->redirectToRoute('client_home');
                }

                return $this->render(
                    'VEVFitnessBundle:Security:confirmation_email.html.twig',
                    array(
                        'form' => $form->createView(),
                        'title' => 'Смена пароля',

                    )
                );
            }
        }

        // пользователь не подтвержден
        // перенаправляем на главную станицу
        return $this->redirectToRoute('vev_fitness_homepage');
    }
}