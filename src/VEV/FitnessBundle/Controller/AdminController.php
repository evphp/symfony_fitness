<?php

namespace VEV\FitnessBundle\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Role;
use VEV\FitnessBundle\Entity\Workout;
use VEV\FitnessBundle\Entity\Subscription;
 
class AdminController extends Controller
{
    public function indexAction()
    {
        $countActiveUsers = $this->getDoctrine()
            ->getRepository(User::class)
            ->countActive();

        $countNotActiveUsers = $this->getDoctrine()
            ->getRepository(User::class)
            ->countNotActive();

        $countGroups = $this->getDoctrine()
            ->getRepository(Workout::class)
            ->count();

        $countSubscriptions = $this->getDoctrine()
            ->getRepository(Subscription::class)
            ->countWitActiveUsers();


        return $this->render('VEVFitnessBundle:Admin:index.html.twig', array(
            'menu_item' => '',
            'active_users_count' => $countActiveUsers,
            'not_active_users_count' => $countNotActiveUsers,
            'groups_count' => $countGroups,
            'subscriptions_count' => $countSubscriptions,
        ));
    }

    public function groupsAction()
    {
        $groups = $this->getDoctrine()
            ->getRepository(Workout::class)
            ->findBy(array(), array('createdAt'=>'desc'));

        // в шаблоне показываем общее кол-во подписок, включая неактивных клиентов
        return $this->render('VEVFitnessBundle:Admin:groups.html.twig', array(
            'menu_item' => 'groups',
            'groups' => $groups
        ));
    }

    public function clientsAction()
    {
        $clients = $this->getDoctrine()
            ->getRepository(User::class)
            ->getClients();

        // $em = $this->getDoctrine()->getManager();
        // $repository = $this->getDoctrine()->getRepository(User::class);
        // $query = $repository->createQueryBuilder('u')

        $arrClients = [];
        foreach ($clients as $client) {

            //$show_url = $this->generateUrl('admin_сlient_show', array('id' => $client->getId()));

            $arrClients[] = [
                'id' => $client->getId(),
                'email' => $client->getEmail(),
                'username' => $client->getUsername(),
                'image' => $client->getInfo()->getImage()
            ];
        }

        return $this->render('VEVFitnessBundle:Admin:clients.html.twig', array(
            'menu_item' => 'clients',
            'clients'   => $arrClients,
        ));
    }
    public function notificationsAction()
    {
        // получаем группы только с подписками
        $groups = $this->getDoctrine()
            ->getRepository(Workout::class)
            ->getSubscribed();

        return $this->render('VEVFitnessBundle:Admin:notifications.html.twig', array(
            'menu_item' => 'notify',
            'groups' => $groups
        ));
    }

    public function clientShowAction($id)
    {
        $user = null;
        // $em = $this->getDoctrine()->getManager();

        if ($id) {
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(array('id' => $id));
        }

        return $this->render('VEVFitnessBundle:Admin:client_show.html.twig', array(
            'menu_item' => '',
            'client' => $user
        ));
    }
}