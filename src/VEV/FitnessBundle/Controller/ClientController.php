<?php

namespace VEV\FitnessBundle\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Role;
use VEV\FitnessBundle\Entity\Workout;
use VEV\FitnessBundle\Entity\Subscription;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VEV\FitnessBundle\Form\UserPasswordType;
use VEV\FitnessBundle\Form\SubscriptionType;

class ClientController extends Controller
{
    public function indexAction(UserInterface $user)
    {
        $groups = $this->getDoctrine()
            ->getRepository(Workout::class)
            ->findBy(array(), array('createdAt'=>'desc'));

        // $sub = $this->getDoctrine()
        //     ->getRepository(Subscription::class)
        //     ->getSubsription(4, 4);

        $arrGroups = [];
        foreach ($groups as $group) {

            // пробуем извелечь сущность Subscription из репозитория
            $subscription = $this->getDoctrine()
                ->getRepository(Subscription::class)
                ->getSubsription($user->getId(), $group->getId());

            // если пользователь ранее не был подписан,
            // то создаем новую подписку
            if (is_null($subscription)) {
                $subscription = new Subscription($user, $group);
            }

            // создаем формы с уникальными атрибутами (name, id)
            $uniqueFormName = 'subscription_' . $group->getId();
            $form = $this->get('form.factory')->createNamed($uniqueFormName, SubscriptionType::class, $subscription);

            $arrGroups[] = [
                'id' => $group->getId(),
                'name' => $group->getName(),
                'trainer' => $group->getTrainer(),
                'description' => $group->getDescription(),
                'createdAt' => $group->getCreatedAt(),
                'form' => $form->createView(),
            ];
        }

        return $this->render('VEVFitnessBundle:Client:groups.html.twig', array(
            'menu_item' => '',
            'groups' => $arrGroups
        ));
    }

    public function accountAction(UserInterface $user)
    {
        // $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('VEVFitnessBundle:Client:account.html.twig', array(
            'menu_item' => 'account',
            'client' => $user,
        ));
    }

    public function updatePassAction(Request $request, UserInterface $user)
    {

        $form = $this->createForm(UserPasswordType::class, $user);

        // обрабатываем запрос
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            // шифруем и добавляем пароль пользователя
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('client_account');
        }

        return $this->render('VEVFitnessBundle:Client:edit_password.html.twig', array(
            'menu_item' => '',
            'form' => $form->createView(),
            'title' => 'Смена пароля',
        ));
    }

    public function ajaxSubscribeAction(Request $request, UserInterface $user, Workout $workout)
    {
        $status ='failed';

        if ($request->isXmlHttpRequest()) {

            $subscription = $this->getDoctrine()
                ->getRepository(Subscription::class)
                ->getSubsription($user->getId(), $workout->getId());

            if (is_null($subscription)) {
                $subscription = new Subscription($user, $workout);
            }

            $uniqueFormName = 'subscription_' . $workout->getId();
            $form = $this->get('form.factory')->createNamed($uniqueFormName, SubscriptionType::class, $subscription);
            //$form->submit($request->request->get($form->getName()));

            $form->handleRequest($request);

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                if ($subscription->getType()) {
                    $em->persist($subscription);
                } else {
                    $em->remove($subscription);
                }
                $em->flush();
                $status ='success';
            } else {
                // $validator = $this->get('validator');
                // $errorsValidator = $validator->validate($subscription);
                // $errors = [];
                // foreach ($errorsValidator as $error) {
                //     array_push($errors, $error->getMessage());
                // }
                $status = 'form_not_valid';
            }
            return new Response(json_encode(array('status' => $status)));
        }
        return new Response(json_encode(array('status' => $status)));
    }
}