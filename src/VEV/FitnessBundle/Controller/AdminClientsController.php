<?php

namespace VEV\FitnessBundle\Controller;

use VEV\FitnessBundle\Entity\User;
use VEV\FitnessBundle\Entity\Role;
use VEV\FitnessBundle\Form\UserType;
use VEV\FitnessBundle\Entity\UserInfo;
use VEV\FitnessBundle\Entity\Confirmation;
use Symfony\Component\HttpFoundation\File\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use VEV\FitnessBundle\Service\ImageUploader;

class AdminClientsController extends Controller
{
    /**
     * @Route("/admin/clients/create", name="admin_client_create")
     */
    public function createAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $user->setSalt(md5(time()));
        $user->setPlainPassword(rand(1000000, 2000000));

        // необходимо создать объект типа UserInfo чтобы работала валидация
        $user->setInfo(new UserInfo());

        $form = $this->createForm(UserType::class, $user);
        $form->remove('isActive');

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            // $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            // $user->setPassword($password);
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 5) добавляем пользователю его роль
            $userRole = $this->getDoctrine()->getRepository(Role::class)
                ->findOneBy(array('name' => 'ROLE_USER'));
            $user->addUserRole($userRole);

            # сохраняем изображение
            $info = $user->getInfo();
            $image = $info->getImageTmp();
            if ($image) {
                $imageName = $this->get('app.image_uploader')->upload($image);
                $info->setImage($imageName);
                $user->setInfo($info);
            }

            // 5. save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // 6. отправка эмейла с линком подверждения
            $this->sendCofirmationEmail($user->getEmail(), $user->getId());

            return $this->redirectToRoute('admin_clients');

        }

        return $this->render(
            'VEVFitnessBundle:Admin:client_create_form.html.twig',
            array(
                'form' => $form->createView(),
                'menu_item' => '',
            )
        );
    }

    /**
     * @Route("/admin/clients/{id}/update", name="admin_client_update")
     */
    public function updateAction(Request $request, User $user)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->remove('email');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            # сохраняем изображение
            $info = $user->getInfo();
            $image = $info->getImageTmp();
            if ($image) {
                $imageName = $this->get('app.image_uploader')->upload($image);
                $info->setImage($imageName);
                $user->setInfo($info);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_сlient_show', array('id' => $user->getId()));
        }

        return $this->render(
            'VEVFitnessBundle:Admin:client_update_form.html.twig', 
            array(
                'form' => $form->createView(),
                'menu_item' => '',
                'user_id' => $user->getId(),
                'image' => $user->getInfo()->getImage()
            )
        );
    }

    public function sendCofirmationEmail($emailTo, $userId)
    {
        // создаем записить в БД с токеном и id пользователя
        // для подтверждения емейла
        $token = md5($userId.$emailTo);

        $confirmation = new Confirmation();
        $confirmation->setUserId($userId);
        $confirmation->setToken($token);
        $em = $this->getDoctrine()->getManager();
        $em->persist($confirmation);
        $em->flush();

        // отправляем письмо подтверждения регистрации
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($emailTo)
            ->setBody(
                $this->renderView(
                    'VEVFitnessBundle:Emails:user_confirmation.html.twig',
                    array('token' => $token)
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $this->get('mailer')->send($message);
    }
}