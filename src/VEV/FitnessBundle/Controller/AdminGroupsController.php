<?php

namespace VEV\FitnessBundle\Controller;

use VEV\FitnessBundle\Entity\Workout;
use VEV\FitnessBundle\Form\WorkoutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminGroupsController extends Controller
{
    /**
     * @Route("/admin/groups/create", name="admin_group_create")
     */
    public function createAction(Request $request)
    {
        // 1. build the form
        $workout = new Workout();

        $form = $this->createForm(WorkoutType::class, $workout);

        // 2. handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // 3. save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($workout);
            $em->flush();

            return $this->redirectToRoute('admin_groups');
        }

        return $this->render(
            'VEVFitnessBundle:Admin:group_form.html.twig',
            array(
                'form' => $form->createView(),
                'menu_item' => '',
                'action_title' => 'Создать группу'
            )
        );
    }

    /**
     * @Route("/admin/groups/{id}/update", name="admin_group_update")
     */
    public function updateAction(Request $request, Workout $workout)
    {
        $form = $this->createForm(WorkoutType::class, $workout);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_groups');
        }

        return $this->render('VEVFitnessBundle:Admin:group_form.html.twig', [
            'form' => $form->createView(),
            'menu_item' => '',
            'action_title' => 'Редактировать группу'
        ]);
    }

    /**
     * @Route("/admin/groups/{workout}/delete", name="admin_group_delete")
     */
    public function deleteAction(Request $request, Workout $workout)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($workout);
        $em->flush();

        return $this->redirectToRoute('admin_groups');
    }

    public function ajaxNotificationAction(Request $request)
    {
        $result = [
            'status' => 'failed',
            'errors' => '',
        ];

        if ($request->isXmlHttpRequest()) {

            $groupId = $request->get('group_id');
            $emailText = $request->get('email_txt');
            $smsText = $request->get('sms_txt');

            # 1. валидация полученных значений
            $isValid = true;

            if ($isValid) {

                # передаем сообщения менеджеру для отправки в очередь в зависимости от типа email/sms
                $this->get('app.notification_manager')->task($groupId, $emailText, $smsText);

                $result['status'] ='success';
            } else {
                $result['status'] ='form_not_valid';
                $result['errors'] ='Текст не прошел валидацию или отсутствует.';
            }
            return new Response(json_encode($result));
        }
        return new Response(json_encode($result));
    }
}