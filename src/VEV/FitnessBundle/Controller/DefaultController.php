<?php

namespace VEV\FitnessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $title = 'FITNESS';

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
           return $this->redirectToRoute('admin_home');
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('client_home');
        }

        return $this->render('VEVFitnessBundle:Default:index.html.twig', [
            'title' => $title
        ]);
    }
}
